const {Add} = require("./index.js")

test("Returns zero on empty string", () => {
    expect(Add("")).toBe(0)
})

test("Returns simple integer", () => {
    expect(Add("5")).toBe(5)
})

test("Returns two-digit integer", () => {
    expect(Add("11")).toBe(11)
})

test("Returns sum of two integers", () => {
    expect(Add("3,9")).toBe(12)
})

test("Returns sum of three integers", () => {
    expect(Add("7,53,82")).toBe(142)
    expect(Add("1,2,5")).toBe(8)
})

test("Handles newlines", () => {
    expect(Add("1\n,2,3")).toBe(6)
    expect(Add("1,\n2,4")).toBe(7)
})

test("Handles control codes", () => {
    expect(Add("//_\n4_9_22")).toBe(35)
    expect(Add("//;\n1;3;4")).toBe(8)
    expect(Add("//$\n1$2$3")).toBe(6)
    expect(Add("//@\n2@3@8")).toBe(13)
})

test("Throws on negative numbers", () => {
    const errorBase = "Negatives not allowed. Negative number(s) supplied: "
    expect(() => Add("//&\n-4")).toThrowError(errorBase + "-4")
    expect(() => Add("//&\n22&-3&-11&8")).toThrowError(errorBase + "-3,-11")
})

test("Ignores numbers bigger than 1000", () => {
    expect(Add("1001")).toBe(0)
    expect(Add("2,1001")).toBe(2)
    expect(Add("1000,5,1001,1002,9")).toBe(1014)
})

test("Handles multicharacter delimiters", () => {
    expect(Add("//$_$\n5$_$22$_$14")).toBe(41)
    expect(Add("//!!!\n20\n!!!30\n!!!50")).toBe(100)
})