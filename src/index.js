/**
 * Adds the given numbers, specified as a delimiter-separated string.
 * 
 * @method
 * @param {string} numbers Specification of the numbers to add.
 *      May begin with a control code of the form "//[delimiter]\n".
 *      Continues with a string of positive integers separated
 *      by the specified delimiter (or a comma if unspecified).
 *      Newlines at the start or end of a number are ignored,
 *      as are any numbers bigger than 1000.
 * @return {number} The sum of the specified numbers, or 0 if given an empty string.
 * @throws {Error} If any of the numbers provided are negative.
 */
exports.Add = (numbers) => {
    if(numbers === "")
    {
        return 0
    }

    // default values - for parts 1 and 2 where there were no control codes
    let delimiter = ","
    let actualNumbers = numbers
    
    // handle control codes
    if(numbers[0] === "/")
    {
        const endOfControlCode = numbers.indexOf("\n")
        delimiter = numbers.substring(2, endOfControlCode) // strip leading "//" and trailing newline
        actualNumbers = numbers.substring(endOfControlCode + 1)
    }

    const {sum, negatives} = actualNumbers
        .split(delimiter)
        .map(numberString => Number.parseInt(numberString, 10)) // parseInt ignores leading and trailing whitespace
        .reduce(
            (previous, current) => ({
                sum: current > 1000 ? previous.sum : previous.sum + current,
                negatives: current < 0 ? previous.negatives.concat(current) : previous.negatives
            }),
            {sum: 0, negatives: []}
        )

    if(negatives.length)
    {
        throw new Error(`Negatives not allowed. Negative number(s) supplied: ${negatives}`)
    }

    return sum
}
