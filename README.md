# String Calculator

Simple Node.js-based String Calculator

## To Install

```bash
% npm install
```

## To Run Tests

```bash
% npm test
```

## To Generate Documentation

```bash
% npm run doc
# Resulting HTML will be in docs/index.html
```
